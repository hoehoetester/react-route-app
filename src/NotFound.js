import React from 'react';

const NotFound = (location) => {
    return (
        <div>
            <h2>page not found <code>{location.location.pathname}</code></h2>
        </div>
    );
};

export default NotFound;
