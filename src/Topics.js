import React from 'react';
import {
    Route,
    Link
} from 'react-router-dom';

const Topics = ({ match }) => {
    return (
        <div>
            <h2>Topics</h2>
            <ul>
                <li>
                    <Link to={`${match.url}/rendering`}>rendring with react</Link>
                </li>
                <li>
                    <Link to={`${match.url}/component`}>component</Link>
                </li>
                <li>
                    <Link to={`${match.url}/props`}>props</Link>
                </li>
            </ul>

            <Route path={`${match.url}/:topicId`} component={Topic} />
            <Route exact path={match.url} render={() => (
                <h3>please select a topic</h3>)} />
        </div>
    );
};

const Topic = ({ match }) => {
    return (
        <div>
            <h3>{match.params.topicId}</h3>
        </div>
    );
};

export default Topics;