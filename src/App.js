import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from 'react-router-dom';
import Home from './Home';
import About from './About';
import Topics from './Topics';
import NotFound from './NotFound';
import './App.css';

const App = () => (
  <Router>
    <div>
      <nav>
        <NavLink to="/" >Home</NavLink>
        <NavLink to="/about">About</NavLink>
        <NavLink to="/topics" >Topics</NavLink>
      </nav>

      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/topics" component={Topics} />
        <Route component={NotFound} />
      </Switch>
    </div>
  </Router>
);

export default App;
